//
// Created by Dmitrii Borovoi on 20.05.18.
//

#include <cassert>
#include <cv.hpp>
#include "BlackPixels.h"

YPP::BlackPixels::BlackPixels(const cv::Mat &frame, unsigned int _row, bool _borders_are_black) :
        row(_row), borders_are_black(_borders_are_black) {
    assert(frame.cols > 0);
    assert(frame.rows > static_cast<int>(row));
    for (int col = 0; col < frame.cols; col++) {
        if (frame.at<uchar>(row, col) < 255 || (borders_are_black && (col == frame.cols - 1 || col == 0))) {
            this->emplace_back(col);
        }
    }
}

void YPP::BlackPixels::draw(cv::Mat &frame) const {
    for (auto x:*this) {
        cv::circle(frame, {static_cast<int>(x), static_cast<int>(row)}, 1, {0, 0, 255});
    }
}

std::ostream &operator<<(std::ostream &os, const YPP::BlackPixels &pixels) {
    os << "BlackPixels: " << &pixels << "{";
    for (auto i = pixels.cbegin(); i < pixels.cend(); ++i) {
        os << *i << ", ";
    }
    os << "}" << std::endl;
    return os;
}

unsigned int YPP::BlackPixels::getRow() const {
    return row;
}

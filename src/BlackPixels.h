//
// Created by Dmitrii Borovoi on 20.05.18.
//

#pragma once

#include <opencv2/core/mat.hpp>
#include <ostream>

namespace YPP {

    struct BlackPixels : public std::vector<unsigned int> {
        BlackPixels(const cv::Mat &frame, unsigned int row, bool borders_are_black);

        void draw(cv::Mat &frame) const;

        friend std::ostream &operator<<(std::ostream &os, const BlackPixels &pixels);

    private:
        unsigned int row;
    public:
        unsigned int getRow() const;

    private:
        bool borders_are_black;
    };
}
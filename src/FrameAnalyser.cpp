//
// Created by Dmitrii Borovoi on 21.05.18.
//

#include <cassert>
#include <cv.hpp>
#include "FrameAnalyser.h"

YPP::FrameAnalyser::FrameAnalyser(cv::Mat frame, unsigned int
threshold,
                                  unsigned int max_noise_distance,
                                  bool borders_are_black
) {
    assert(frame.rows > 0);
    assert(frame.cols > 0);
    cv::Mat frame_binary;
    cv::cvtColor(frame, frame_binary, cv::COLOR_RGB2GRAY);
    cv::threshold(frame_binary, frame_binary, threshold, 255, cv::THRESH_BINARY);
    for (unsigned int row = 0; row < static_cast<unsigned int>(frame.rows); ++row) {
        this->push_back(YPP::WidePixels(YPP::BlackPixels(frame_binary, row, borders_are_black), max_noise_distance));
    }
}

std::vector<YPP::PianoKey>
YPP::FrameAnalyser::white_keys(unsigned int min_keys_number, double min_distance_noise, double min_wide_noise) const {
    std::vector<YPP::PianoKey> white_keys;
    std::vector<WidePixels> wide_pixels(*this);
    wide_pixels.erase(std::remove_if(wide_pixels.begin(), wide_pixels.end(),
                                     [min_distance_noise, min_wide_noise, min_keys_number](const WidePixels &member) {
                                         return member.size() < min_keys_number ||
                                                member.distance_noise() > min_distance_noise ||
                                                member.wide_noise() > min_wide_noise;
                                     }),
                      wide_pixels.end());
    if (!wide_pixels.empty()) {
        unsigned int row_avg = 0;
        unsigned int distance_avg = 0;
        std::for_each(wide_pixels.cbegin(), wide_pixels.cend(), [&row_avg, &distance_avg](const WidePixels &member) {
            row_avg += member.getRow();
            distance_avg += member.distance_avg();
        });
        row_avg /= wide_pixels.size();
        distance_avg /= wide_pixels.size();
        std::for_each(wide_pixels.cbegin()->cbegin(), wide_pixels.cbegin()->cend(),
                      [&white_keys, row_avg, distance_avg](auto member) {
                          white_keys.emplace_back(cv::Point2d((member.first + distance_avg / 2), row_avg),
                                                  YPP::PianoKey::Type::WHITE);
                      });
    }
    return white_keys;
}

std::vector<YPP::PianoKey>
YPP::FrameAnalyser::black_keys(unsigned int min_keys_number, double max_keeped_keys_when_delete_white,
                               double min_wide_noise) const {
    std::vector<YPP::PianoKey> black_keys;
    std::vector<WidePixels> wide_pixels(*this);
    wide_pixels.erase(std::remove_if(wide_pixels.begin(), wide_pixels.end(),
                                     [max_keeped_keys_when_delete_white, min_wide_noise, min_keys_number](
                                             WidePixels &member) {
                                         return member.delete_small_pixels(
                                                 static_cast<unsigned int>(member.wide_avg())) >
                                                max_keeped_keys_when_delete_white ||
                                                member.size() < min_keys_number ||
                                                member.wide_noise() > min_wide_noise;
                                     }),
                      wide_pixels.end());
    if (!wide_pixels.empty()) {
        unsigned int row_avg = 0;
        std::for_each(wide_pixels.cbegin(), wide_pixels.cend(), [&row_avg](const WidePixels &member) {
            row_avg += member.getRow();
        });
        row_avg /= wide_pixels.size();
        std::for_each(wide_pixels.cbegin()->cbegin(), wide_pixels.cbegin()->cend(),
                      [&black_keys, row_avg](auto member) {
                          black_keys.emplace_back(cv::Point2d(member.first, row_avg), YPP::PianoKey::Type::BLACK);
                      });
    }
    return black_keys;
}

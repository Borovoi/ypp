//
// Created by Dmitrii Borovoi on 21.05.18.
//

#pragma once

#include "PianoKey.h"
#include <opencv2/core/mat.hpp>
#include "WidePixels.h"

namespace YPP {
    class FrameAnalyser : std::vector<WidePixels> {
    public:
        FrameAnalyser(cv::Mat frame, unsigned int threshold, unsigned int max_noise_distance, bool borders_are_black);

        vector <PianoKey>
        white_keys(unsigned int min_keys_number, double min_distance_noise, double min_wide_noise) const;

        vector <PianoKey>
        black_keys(unsigned int min_keys_number, double max_keeped_keys_when_delete_white, double min_wide_noise) const;
    };
}
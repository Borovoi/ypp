//
// Created by Dmitrii Borovoi on 27.05.18.
//

#include "PianoAnalyser.h"
#include <algorithm>

namespace YPP {


PianoAnalyser::PianoAnalyser(const unsigned int match_each_other_confirm):
match_each_other_confirm(match_each_other_confirm) {

}


const bool PianoAnalyser::feed_founded_keys(const std::vector<PianoKey> &white_keys,
                                           const std::vector<PianoKey> &black_keys) {

    if (this->confirmed()){
        return true;
    }
    if (white_keys.empty() || black_keys.empty()) {
        match_each_other = 0;
        return false;
    }
    if (this->empty()) {
        this->insert(this->end(), white_keys.begin(), white_keys.end());
        this->insert(this->end(), black_keys.begin(), black_keys.end());
        std::sort(this->begin(), this->end(), [](PianoKey key_one, PianoKey key_two){
            return (key_one.x < key_two.x);
        });
        match_each_other++;
        return this->confirmed();
    }
    if (this->size() != (white_keys.size() + black_keys.size())){
        this->clear();
        match_each_other = 0;
    }
    //TODO: Добавить проверку на то что пришли клавиши с другими координатами
    match_each_other++;
    return this->confirmed();
}


const bool PianoAnalyser::confirmed() const {
    const bool result = (match_each_other == match_each_other_confirm);
    return result;
}


std::ostream& operator<<(std::ostream& os, const PianoAnalyser &piano_analyser) {
    os << "PianoAnalyser: " << &piano_analyser << "{";
    for (auto i = piano_analyser.cbegin(); i < piano_analyser.cend(); ++i) {
        os << "(" << i->x << ", " << i->y << ")" << ", ";
    }
    os << "}" << std::endl;
    return os;
}

    void PianoAnalyser::init_keys(const cv::Mat &frame) {
        cv::Mat frame_gray;
        cv::cvtColor(frame, frame_gray, cv::COLOR_RGB2GRAY);
        for (auto key = this->begin(); key < this->end(); key++)
        {
            key->init(frame_gray);
        }
        is_init_flag = true;
    }

    const bool PianoAnalyser::is_init() const {
        return is_init_flag;
    }

    void PianoAnalyser::compare(const cv::Mat &frame) {
        cv::Mat frame_gray;
        cv::cvtColor(frame, frame_gray, cv::COLOR_RGB2GRAY);
        for (auto key = this->begin(); key < this->end(); key++)
        {
            key->compare(frame_gray);
        }
    }


} // namespace
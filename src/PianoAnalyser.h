//
// Created by Dmitrii Borovoi on 27.05.18.
//

#pragma once

#include "PianoKey.h"

namespace YPP {

    class PianoAnalyser : public std::vector<PianoKey> {

    public:

        explicit PianoAnalyser(const unsigned int match_each_other_confirm);

        const bool feed_founded_keys(const std::vector<PianoKey> &white_keys, const std::vector<PianoKey> &black_keys);

        const bool confirmed() const;

        const bool is_init() const;

        void compare(const cv::Mat& frame);

        void init_keys(const cv::Mat& frame);

        friend std::ostream& operator<<(std::ostream &os, const PianoAnalyser &piano_analyser);

    private:

        const unsigned int match_each_other_confirm;

        bool is_init_flag = false;

        unsigned int match_each_other;
        
    };
}


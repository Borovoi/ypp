//
// Created by Dmitrii Borovoi on 26.05.18.
//

#include "PianoKey.h"
#include <iostream>

YPP::PianoKey::PianoKey(cv::Point2d point, YPP::PianoKey::Type type) :
        cv::Point2d(point),
        type(type) {

}

YPP::PianoKey::PianoKey() :
        cv::Point2d(),
        type(YPP::PianoKey::Type::UNDEFINED) {

}

void YPP::PianoKey::init(const cv::Mat &frame) {
    init_color = std::abs(static_cast<int>(frame.at<char>(this->y,this->x)));
}

void YPP::PianoKey::compare(const cv::Mat &frame) {
    int result = std::abs(init_color - std::abs(static_cast<int>(frame.at<char>(this->y,this->x))));
    if (result > diff_to_be_pressed){
        pressed++;
    } else {
        pressed = 0;
    }
}

const unsigned int YPP::PianoKey::frames_pressed() const {
    return pressed;
}



//
// Created by Dmitrii Borovoi on 26.05.18.
//

#pragma once

#include <cv.hpp>

namespace YPP {
    class PianoKey : public cv::Point2d {
    public:

        PianoKey();

        enum Type {
            BLACK, WHITE, UNDEFINED
        };

        PianoKey(cv::Point2d point, Type type);

        void init(const cv::Mat& frame);

        const unsigned int frames_pressed() const;

        void compare(const cv::Mat& frame);

    private:

        Type type;

        int init_color = 0;

        int diff_to_be_pressed = 20;

        unsigned int pressed = 0;
    };


}
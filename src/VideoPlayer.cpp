//
// Created by Dmitrii Borovoi on 27.05.18.
//

#include "VideoPlayer.h"
#include <opencv2/opencv.hpp>

YPP::VideoPlayer::VideoPlayer(std::string _file_name) :
        cap(std::make_unique<cv::VideoCapture>(_file_name)) {
    if (this->cap->isOpened()) {
        *this->cap >> this->frame;
    }
}

const cv::Mat YPP::VideoPlayer::get_frame() const {
    return this->frame;
}

void YPP::VideoPlayer::next_frame() {
    if (this->cap->isOpened()) {
        *this->cap >> this->frame;
    }
}

void YPP::VideoPlayer::show() const {
    if (!this->frame.empty()) {
        cv::imshow("VideoPlayer::frame", this->frame);
    }
}

void YPP::VideoPlayer::draw_piano_keys(const std::vector<YPP::PianoKey> &keys) {
    for (std::vector<YPP::PianoKey, std::allocator<YPP::PianoKey>>::const_iterator key = keys.cbegin();
         key < keys.cend(); key++) {
        cv::circle(this->frame, *key, key->frames_pressed(), {100, 100, 100});
    }
}

const double YPP::VideoPlayer::progress() const {
    const double result = cap->get(cv::CAP_PROP_POS_FRAMES) /
                    (cap->get(cv::CAP_PROP_FRAME_COUNT));
    assert(result >= 0 && result <= 1);
    return result;
}
//
// Created by Dmitrii Borovoi on 27.05.18.
//

#pragma once

#include <string>
#include <memory>
#include <opencv2/videoio.hpp>
#include "FrameAnalyser.h"
#include "PianoAnalyser.h"

namespace YPP {
    class VideoPlayer {

    public:

        explicit VideoPlayer(std::string _file_name);

        const cv::Mat get_frame() const;

        void next_frame();

        void show() const;

        const double progress() const;

        VideoPlayer() = delete;

        VideoPlayer(VideoPlayer &) = delete;

        VideoPlayer &operator=(VideoPlayer &) = delete;

        void draw_piano_keys(const std::vector<PianoKey> &keys);

    private:
        std::unique_ptr<cv::VideoCapture> cap;

        cv::Mat frame;

    };

}


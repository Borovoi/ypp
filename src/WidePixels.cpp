//
// Created by Dmitrii Borovoi on 20.05.18.
//

#include <cv.hpp>
#include "WidePixels.h"

YPP::WidePixels::WidePixels(const BlackPixels &black_pixels, unsigned int max_noise_distance) :
        row(black_pixels.getRow()), max_noise_distance(max_noise_distance) {
    if (black_pixels.empty()) {
        //DO NOTHING
    } else if (black_pixels.size() == 1) {
        this->emplace_back(*black_pixels.cbegin(), 1);
    } else if (black_pixels.size() > 1) {
        auto begin_of_wide_pixel = black_pixels.cbegin();
        for (auto i = begin_of_wide_pixel + 1; i < black_pixels.cend(); i++) {
            auto item_prev = i - 1;

            if (*i - *item_prev > max_noise_distance) {
                this->emplace_back(
                        (*item_prev + *begin_of_wide_pixel) / 2, *item_prev - *begin_of_wide_pixel);
                begin_of_wide_pixel = i;
            }
        }
        this->emplace_back((*(black_pixels.cend() - 1) + *begin_of_wide_pixel) / 2,
                           *(black_pixels.cend() - 1) - *begin_of_wide_pixel);
    }
}

void YPP::WidePixels::draw(cv::Mat &frame) const {
    for (auto x:*this) {
        cv::circle(frame, {static_cast<int>(x.first), static_cast<int>(row)}, x.second, {255, 0, 0});
    }
}

std::ostream &operator<<(std::ostream &os, const YPP::WidePixels &_object) {
    os << "WidePixels: " << &_object << "{";
    for (auto i = _object.cbegin(); i < _object.cend(); ++i) {
        os << "(" << i->first << ", " << i->second << "), ";
    }
    os << "}:" << std::endl;
    return os;
}

double YPP::WidePixels::wide_noise() const {
    double min = 0;
    double max = 0;
    if (this->empty()) {
        min = max = 0;
    } else if (this->size() == 1) {
        min = max = this->begin()->second;
    } else if (this->size() > 1) {
        min = max = this->begin()->second;
        for (auto wide_pixel = this->cbegin() + 1; wide_pixel < this->cend(); wide_pixel++) {
            if (wide_pixel->second > max)
                max = wide_pixel->second;
            if (wide_pixel->second < min)
                min = wide_pixel->second;
        }
    }
    if (max == 0) {
        return 0;
    }
    return (max - min) / max;
}

double YPP::WidePixels::wide_avg() const {
    double avg = 0;
    if (this->empty()) {
        avg = 0;
    } else if (this->size() == 1) {
        avg = this->begin()->second;
    } else if (this->size() > 1) {
        avg = this->begin()->second;
        for (auto wide_pixel = this->cbegin() + 1; wide_pixel < this->cend(); wide_pixel++) {
            avg += wide_pixel->second;
        }
        avg /= this->size();
    }

    return avg;
}

double YPP::WidePixels::distance_noise() const {
    double min = 0;
    double max = 0;
    if (this->size() <= 2) {
        min = max = 0;
    } else if (this->size() == 2) {
        min = max = (this->cbegin() + 1)->first - this->cbegin()->first;
    } else if (this->size() > 2) {
        min = max = (this->cbegin() + 1)->first - this->cbegin()->first;
        for (auto wide_pixel = this->cbegin() + 2; wide_pixel < this->cend(); wide_pixel++) {
            unsigned int __distance = wide_pixel->first - (wide_pixel - 1)->first;
            if (__distance > max)
                max = __distance;
            if (__distance < min)
                min = __distance;
        }
    }
    if (max == 0) {
        return 0;
    }
    return (max - min) / max;
}

double YPP::WidePixels::distance_avg() const {
    double avg = 0;
    if (this->size() <= 2) {
        avg = 0;;
    } else if (this->size() == 2) {
        avg = (this->cbegin() + 1)->first - this->cbegin()->first;
    } else if (this->size() > 2) {
        avg = (this->cbegin() + 1)->first - this->cbegin()->first;
        for (auto wide_pixel = this->cbegin() + 2; wide_pixel < this->cend(); wide_pixel++) {
            unsigned int __distance = wide_pixel->first - (wide_pixel - 1)->first;
            avg += __distance;
        }
        avg /= this->size();
    }
    return avg;
}

unsigned int YPP::WidePixels::getRow() const {
    return row;
}

double YPP::WidePixels::delete_small_pixels(unsigned int _size) {
    if (this->empty()) {
        return 1.0;
    }
    float size_before_deleting = this->size();
    this->erase(std::remove_if(this->begin(), this->end(),
                               [_size](const std::pair<unsigned int, unsigned int> &member) {
                                   return member.second < _size;
                               }),
                this->end());
    return this->size() / size_before_deleting;
}


//
// Created by Dmitrii Borovoi on 20.05.18.
//

#pragma once

#include "BlackPixels.h"

namespace YPP {

    class WidePixels : public std::vector<std::pair<unsigned int, unsigned int>> {
    public:
        WidePixels(const BlackPixels &black_pixels, unsigned int max_noise_distance);

        double delete_small_pixels(unsigned int _size);

        double wide_noise() const;

        double wide_avg() const;

        double distance_noise() const;

        double distance_avg() const;

        void draw(cv::Mat &frame) const;

        friend std::ostream &operator<<(std::ostream &os, WidePixels const &_object);

    private:
        unsigned int row;

        unsigned int max_noise_distance;

    public:
        unsigned int getRow() const;
    };
}
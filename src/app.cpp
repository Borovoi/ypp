//
// Created by Dmitrii Borovoi on 02.05.18.
//
#include <opencv2/opencv.hpp>
#include "VideoPlayer.h"

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "Invalid arguments! \nUsage: program path_to_video_file" << std::endl;
        return -1;
    }

    YPP::VideoPlayer video(argv[1]);
    YPP::PianoAnalyser piano(4);
    while (cv::waitKey(10)) {
        std::cout << "Progress: " << video.progress() << std::endl;
        if (!piano.confirmed()){
            YPP::FrameAnalyser frame_info(video.get_frame(), 210, 3, true);
            piano.feed_founded_keys(frame_info.white_keys(15, 0.3, 100), frame_info.black_keys(10, 0.8, 0.3));
            std::cout <<  "Piano size: " << piano.size() << std::endl;
            std::cout << piano << std::endl;
        }
        if (piano.confirmed() && !piano.is_init())
        {
            piano.init_keys(video.get_frame());
        }
        if (piano.confirmed() && piano.is_init())
        {
            piano.compare(video.get_frame());
        }

        video.draw_piano_keys(piano);
        video.show();
        video.next_frame();
    }
    return 0;
}
